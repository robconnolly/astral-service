#!/usr/bin/env python3

import datetime, logging, configparser, easymqtt, time
from astral import Location
from apscheduler.schedulers.background import BackgroundScheduler

logging.basicConfig(level=logging.DEBUG)

config = configparser.ConfigParser()
config.read("astral-service.ini")

mqtt = easymqtt.EasyMQTT(client_id=config['mqtt']['clientid'])
try:
    mqtt.username_pw_set(config['mqtt']['username'], config['mqtt']['password'])
except KeyError:
    logging.info("No MQTT authentication details specified, disabling auth.")
try:
    mqtt.will_set(config['mqtt']['lwt'], 'offline', 0, True)
except KeyError:
    logging.info("No LWT topic specified, disabling LWT.")

@mqtt.connection
def on_connect(client, userdata, flags, rc):
    logging.info("Connected to MQTT broker @ {}:{}, rc={}".format(
        config['mqtt']['host'], int(config['mqtt']['port']), rc))
    try:
        client.publish(config['mqtt']['lwt'], "online", retain=True, qos=0)
    except:
        pass

mqtt.connect(config['mqtt']['host'], int(config['mqtt']['port']))

info = (
    config['location']['name'],
    config['location']['country'],
    float(config['location']['latitude']),
    float(config['location']['longitude']),
    config['location']['timezone'],
    int(config['location']['elevation'])
)

l = Location(info)
l.solar_depression = config['location']['solar_depression']
logging.info("Initialised location: {}".format(info))

scheduler = BackgroundScheduler()

@scheduler.scheduled_job("interval", minutes=int(config['misc']['update_interval']))
def update_solar_position():
    now = datetime.datetime.now()

    az = l.solar_azimuth(now)
    el = l.solar_elevation(now)

    logging.debug("Updating solar position, azimuth={}, elevation={}".format(az, el))
    mqtt.publish(config['mqtt']['basetopic'] + "/sunpos/azimuth", az, retain=True, qos=2)
    mqtt.publish(config['mqtt']['basetopic'] + "/sunpos/elevation", el, retain=True, qos=2)

@scheduler.scheduled_job("cron", hour=0)
def update_solar_times():
    now = datetime.datetime.now()
    
    sun = l.sun(now)

    logmsg = "Updating solar times: "

    for name, dt in iter(sun.items()):
        logmsg += "{}={}, ".format(name, dt)
        mqtt.publish(config['mqtt']['basetopic'] + name, dt.strftime("%s"), retain=True, qos=2)

    logging.info(logmsg[:-2])

if __name__ == "__main__":
    scheduler.start()
    mqtt.loop_start()

    update_solar_position()
    update_solar_times()

    try:
        while True:
            time.sleep(600)
    except KeyboardInterrupt:
        mqtt.loop_stop()
        scheduler.shutdown()


